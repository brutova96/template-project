# Example task

This is test repository to authorize and see how the Autocode works. The Python file with the function template can be found in the task folder.

## Task

## example Function

### Description
Create a Python function named `example` that always return 'False'.

### Function Signature

```python
def example():
    """
    Return False.

    :return: always False.
    """
```

### Note
Please don't change the function's names and input parameters.